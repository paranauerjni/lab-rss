﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Xml;

namespace LABProjetoRSS
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {

        /*Feed feed = new Feed("https://www.rtp.pt/noticias/rss");
        Feed feed2 = new Feed("../../resources/rss.tecmundo.com.br.xml");*/
        Feed feed = new FeedManager().Open("https://www.rtp.pt/noticias/rss");
        // Feed feed2 = new FeedManager().Open("https://www.rtp.pt/noticias/rss");
        Sync sync = new Sync();

        public MainWindow()
        {
            InitializeComponent();
            
            /*
            *  TODO:
            *  - Rever parte de notificações
            *  
            *  hellow a partir do notebook
            *  
            *  RSSs para teste:
            *  - https://www.rtp.pt/noticias/rss
            *  - http://rss.tecmundo.com.br/feed
            *  - https://feeds.bbci.co.uk/portuguese/rss.xml
            *      
            */

            // feed.LoadOffline();
            // new Sync().BindListBox(feed, lb_data);
            // new Sync().HotUpdate(feed, lb_data, 4);
            // sync.HotUpdate(feed, lb_data, 4);
            sync.BindListBox(feed, lb_data);

            // MessageBox.Show(feed.LastTimeUpdated.ToString());
            // feed2.LoadOffline();
            // feed.SaveLocally();
            // Trace.WriteLine(feed.LastTimeUpdated.ToString());

            // MessageBox.Show(feed.GetNumberOfNewIndexes(feed2).ToString() + " out of " + feed.GetNewItems(feed2).Count.ToString());
            // MessageBox.Show(feed.Notifications.Count + " out of " + feed.Items.Count + " have the keywords");

            // new KeyWord().Create("ncis");

            // new FeedManager().Remove("https://www.rtp.pt/noticias/rss");
            foreach (var notification in feed.Notifications)
            {
                MessageBox.Show(notification.Item.Title, "Notificação");
            }

            feed_info.Text = feed.Title + "\n" + feed.Description;
            // new FeedManager().Remove(feed.Path);
            foreach (var f in new FeedManager().Feeds)
            {
                 // MessageBox.Show(f);
            }

            foreach (var k in new KeyWord().Keys)
            {
                // MessageBox.Show(k);
            }

            // var feedManaged = new FeedManager().Create("https://www.rtp.pt/noticias/rss");
            // feed_info.Text = feedManaged.Title + "\n" + feedManaged.Description;
            // MessageBox.Show(feed.GetNumberOfNewIndexes(feed2).ToString() + " out of " + feed.Items.Count.ToString());

            // new FeedManager().Remove("https://www.rtp.pt/noticias/rss");
            // new FeedManager().List();

            // new Sync().BindListBox(new FeedManager().Open("https://www.rtp.pt/noticias/rss"), lb_data);

            // new Sync().BindListBox(feedManaged, lb_data);

            // MessageBox.Show(feed.GetNumberOfNewIndexes(feed2).ToString() + " vs " + feed.Items.Count.ToString());
            // new KeyWord().Create("vacina");

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Browser browser = new Browser();

            if (lb_data.SelectedItem == null)
            {
                return;
            }

            int selectedValue = lb_data.SelectedIndex;

            /*if (String.IsNullOrEmpty(selectedValue))
            {
                MessageBox.Show("Select a valid option", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }*/

            // var item = feed.ItemHandler.Get(selectedValue);
            // MessageBox.Show(selectedValue);
            browser.Url = feed.Items[selectedValue].Link;

            browser.Show();

        }

        private void btn_stopHotUpdate_Click(object sender, RoutedEventArgs e)
        {
            sync.StopHotUpdate();
        }



        /* Handle the NodeInserted event.
        private void MyNodeInsertedEvent(Object source, XmlNodeChangedEventArgs args)
        {
            if (args.Node.Value != null)
            {
                MessageBox.Show("Added" + args.Node.InnerText);
                Console.WriteLine(" with value {0}", args.Node.Value);
            }
            else
            {
                Console.WriteLine("");
            }
        }*/


    }
}
