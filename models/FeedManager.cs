﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace LABProjetoRSS
{
    /// <summary>
    ///     Can manage different Feeds. It could be used when creating a controller for a menu, for instance.
    /// </summary> 
    public class FeedManager : LocalStorageHandler
    {
        public List<string> Feeds { get { return All(); } private set { } }

        public FeedManager()
        {
            Feeds = new List<string>();
            doc = new XmlDocument();
            doc.Load("../../resources/manager/feeds.xml");

            foreach (XmlElement feedLink in GetDataNode().ChildNodes)
            {
                Feeds.Add(feedLink.InnerText);
            }
        }

        protected override void Add(string feed)
        {
            if (Exists(feed))
            {
                return;
            }

            XmlElement feedNode = doc.CreateElement("feed");
            feedNode.InnerText = feed;
            doc.DocumentElement.PrependChild(feedNode);
            doc.Save("../../resources/manager/feeds.xml");
            Feeds.Add(feed);
        }

        /// <summary>
        ///     Creates a new feed
        /// </summary> 
        public Feed Create(string path)
        {
            var feed = new Feed(path);
            Add(path);
            feed.SaveLocally();
            return feed;
        }

        /// <summary>
        ///     Opens an existing feed
        /// </summary> 
        public Feed Open(string path)
        {
            if (Exists(path))
            {
                return new Feed(path);
            }

            return null;
        }

        /// <summary>
        ///     Removes the selected feed from the xml
        /// </summary> 
        private void RemoveFromXml(string path)
        {
            foreach (XmlElement feed in GetDataNode().ChildNodes)
            {
                if (feed.InnerText == path)
                {
                    feed.ParentNode.RemoveChild(feed);
                }
            }
            doc.Save("../../resources/manager/feeds.xml");
        }

        public override bool Remove(string path)
        {
            try
            {
                Feeds.Remove(path);
                RemoveFromXml(path);
                File.Delete(ConvertToLocalPath(path));

                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool Exists(string path)
        {
            return File.Exists(ConvertToLocalPath(path));
        }

        /// <summary>
        ///     Converts the given url to its local address
        /// </summary> 
        private string ConvertToLocalPath(string path)
        {
            return "../../resources/" + new Uri(path).Host + ".xml";
        }
    }
}
