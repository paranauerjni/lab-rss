﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LABProjetoRSS
{
    abstract public class LocalStorageHandler : XmlHandler
    {
        /// <summary>
        ///     Retrieves the data on the xml file
        /// </summary> 
        protected XmlNode GetDataNode()
        {
            XmlNode dataNode = doc.DocumentElement;
            return dataNode;
        }

        /// <summary>
        ///    Retrieves all the items inside data node
        /// </summary> 
        protected List<string> All()
        {
            var response = new List<string>();
            foreach (XmlElement values in GetDataNode().ChildNodes)
            {
                response.Add(values.InnerText);
            }
            return response;
        }

        /// <summary>
        ///    Verifies if a item exists
        /// </summary> 
        public abstract bool Exists(string value);

        /// <summary>
        ///    Adds a new item to the xml data
        /// </summary> 
        protected abstract void Add(string value);

        /// <summary>
        ///     Removes an existing keyword
        /// </summary> 
        public abstract bool Remove(string value);

    }
}
