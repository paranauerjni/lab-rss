﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LABProjetoRSS
{
    /// <summary>
    ///     Its the father of feed and itemhandler. Has a supportive nature. Holds a XmlDocument to prevent duplicated access to a same file.
    /// </summary> 
    public class XmlHandler
    {
        protected XmlDocument doc { get; set; }

    }
}
