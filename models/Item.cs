﻿using System.Xml;

namespace LABProjetoRSS
{
    /// <summary>
    ///     Stands for a single Item. Has the capability to check if contains a password inside it
    /// </summary> 
    public class Item
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string PubDate { get; set; }
        public string Category { get; set; }


        public Item()
        {

        }

        public Item(string title, string desc, string lnk, string pubDate, string category)
        {
            Title = title;
            Description = desc;
            Link = lnk;
            PubDate = pubDate;
            Category = category;
        }

        public Item(XmlNode node)
        {
            Title = node.SelectSingleNode("title").InnerText;
            Description = node.SelectSingleNode("description").InnerText;
            Link = node.SelectSingleNode("link").InnerText;
            PubDate = node.SelectSingleNode("pubDate").InnerText;

            if (node.SelectSingleNode("category") != null)
            {
                Category = node.SelectSingleNode("category").InnerText;
            }
            
        }

        /// <summary>
        ///     Verifies if the Item contains the keyword. Compares with title and description
        /// </summary> 
        public bool Contains(string keyword)
        {
            if (Title.ToLower().Contains(keyword.ToLower()) || Description.ToLower().Contains(keyword.ToLower()))
            {
                return true;
            }
            return false;
        }

    }
}
