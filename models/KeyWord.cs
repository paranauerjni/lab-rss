﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace LABProjetoRSS
{
    /// <summary>
    ///     Provides keywords actions and retrieve them as attribute as well
    /// </summary> 
    class KeyWord : LocalStorageHandler
    {
        public List<string> Keys { get { return All();  } set { } }
        public KeyWord()
        {
            Keys = new List<string>();
            doc = new XmlDocument();
            doc.Load("../../resources/keywords/keywords.xml");
        }

        /// <summary>
        ///    Creates a new keyword
        /// </summary> 
        public void Create(string key)
        {
            Add(key);
        }

        protected override void Add(string key)
        {
            if (Exists(key))
            {
                return;
            }

            XmlElement keyNode = doc.CreateElement("keyword");
            keyNode.InnerText = key;
            doc.DocumentElement.PrependChild(keyNode);
            doc.Save("../../resources/keywords/keywords.xml");
            Keys.Add(key);
        }

        public override bool Exists(string key)
        {
            return Keys.Contains(key);
        }

        public override bool Remove(string key)
        {
            if (!Exists(key))
            {
                return false;
            }

            foreach(XmlElement keyword in GetDataNode().ChildNodes)
            {
                if(keyword.InnerText == key)
                {
                    keyword.ParentNode.RemoveChild(keyword);
                }
            }
            doc.Save("../../resources/keywords/keywords.xml");
            Keys.Remove(key);
            return true;
        }

    }
}
