﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace LABProjetoRSS
{
    /// <summary>
    ///     Handles Item requisitions, such as get, exists and all
    /// </summary> 
    public class ItemHandler : RSSHandler
    {
        public ItemHandler(XmlDocument document)
        {
            doc = document;
        }

        /// <summary>
        ///     Retrieves all the items. The get method retrieves only one element
        /// </summary> 
        public List<Item> All()
        {
            List<Item> responses = new List<Item>();
            foreach (XmlElement item in GetChannelNode().SelectNodes("item"))
            {
                responses.Add(new Item(item));
            }
            return responses;
        }

        /// <summary>
        ///     Verifies if an Item exists, by its title
        /// </summary> 
        public bool Exists(Item itemToSearch)
        {
            foreach (XmlElement item in GetChannelNode().SelectNodes("item"))
            {
                var title = item.SelectSingleNode("title").InnerText;
                if (title == itemToSearch.Title)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     Retrieves the item, by title
        /// </summary> 
        public Item Get(string titleToSearch)
        {
            foreach (XmlElement item in GetChannelNode().SelectNodes("item"))
            {
                var title = item.SelectSingleNode("title").InnerText;
                if (title == titleToSearch)
                {
                    return new Item(item);
                }
            }

            return null;
        }


    }
}
