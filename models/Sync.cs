﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace LABProjetoRSS
{
    /// <summary>
    ///     Sync is used to sync the controllers and views with the model.
    /// </summary> 
    public class Sync
    {
        private bool Continue { get; set; }

        public Sync()
        {
            Continue = true;
        }

        /// <summary>
        ///     Stops the hot updating
        /// </summary> 
        public void StopHotUpdate()
        {
            Continue = false;
        }

        /// <summary>
        ///     Syncs the listbox with the xml data. If the doc is not updated, binds the first load
        /// </summary> 
        public void BindListBox(Feed feed, ListBox lb)
        {
            lb.Items.Clear();
            foreach (var item in feed.Items)
            {
                lb.Items.Add(item.Title);
            }
        }

        /// <summary>
        ///     Updates the listbox every n seconds
        /// </summary> 
        public void HotUpdate(Feed feed, ListBox lb, int sec)
        {
            BindListBox(feed, lb);
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler((sender, e) => DispatcherTimer_Tick(sender, e, feed, lb));
            dispatcherTimer.Interval = new TimeSpan(0, 0, sec);
            dispatcherTimer.Start();
        }

        /// <summary>
        ///     Its triggered by the hotupdate
        /// </summary> 
        private void DispatcherTimer_Tick(object sender, EventArgs e, Feed feed, ListBox lb)
        {
            if (Continue)
            {
                feed.Update();
                BindListBox(feed, lb);
            }
        }
    }
}
