﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace LABProjetoRSS
{
    /// <summary>
    ///     Feed holds the feed data and all the items inside it, sinchronizing automatically
    /// </summary> 
    public class Feed : RSSHandler
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string Language { get; set; }
        public DateTime LastTimeUpdated { get { return System.IO.File.GetLastWriteTime("../../resources/" + new Uri(Path).Host + ".xml"); } }

        public string Path { get; private set; }
        public List<Item> Items { get { return ItemHandler.All(); } private set { } }
        public ItemHandler ItemHandler { get; set; }
        private KeyWord KeyWords { get; set; }
        public List<Notification> Notifications { get { return SearchNotifications(); } }

        public Feed(string path)
        {
            Load(path);
        }

        /// <summary>
        ///     Loads the data online
        ///     <para>PLUS: can run offline xml too, but its preferred to run LoadOffline and use the web url normally</para>
        /// </summary> 
        private void Load(string path)
        {
            Path = path;
            doc = new XmlDocument();
            Update();
            RetrieveFeedData();
            KeyWords = new KeyWord();
        }

        /// <summary>
        ///     Loads the data offline, based on online url
        /// </summary> 
        public void LoadOffline()
        {
            Path = "../../resources/" + new Uri(Path).Host + ".xml";
            doc = new XmlDocument();
            Update();
            RetrieveFeedData();
            KeyWords = new KeyWord();
        }

        /// <summary>
        ///     Reloads the data
        /// </summary> 
        public void Reload()
        {
            Load(Path);
        }

        /// <summary>
        ///     Saves the current xml data locally
        /// </summary> 
        public void SaveLocally()
        {
            var url = new Uri(Path);
            doc.Save("../../resources/" + url.Host + ".xml");
        }

        /// <summary>
        ///     Reloads the ItemHandler and the own rss channel. Basically, synchronizes with the channel again.
        /// </summary> 
        public void Update()
        {
            try
            {
                doc.Load(Path);
                ItemHandler = new ItemHandler(doc);
            }
            catch
            {
                MessageBox.Show("Não foi possível encontrar o caminho: " + Path, "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
                System.Environment.Exit(404);
            }
        }

        /// <summary>
        ///     Retrieves the information about the feed
        /// </summary> 
        private void RetrieveFeedData()
        {
            var channel = GetChannelNode();
            Title = channel.SelectSingleNode("title").InnerText;
            Description = channel.SelectSingleNode("description").InnerText;
            Link = channel.SelectSingleNode("link").InnerText;
            Language = channel.SelectSingleNode("language").InnerText;
        }

        /// <summary>
        ///     Check if there is a notification by the given elements
        ///     <para>Returns a Notification object</para>
        /// </summary> 
        private List<Notification> SearchNotifications()
        {
            var localFeed = new Feed(Path);
            localFeed.LoadOffline();

            var notifications = new List<Notification>();
            foreach(var item in GetNewItems(localFeed))
            {
                foreach (var keyword in KeyWords.Keys)
                {
                    if (item.Contains(keyword))
                    {
                        notifications.Add(new Notification(item, keyword));
                    }
                }
            }
            return notifications;
        }

        /// <summary>
        ///     Retrieves a list of new items, based on the given feed. Basically, it needs the old feed to see what are the news. So, the feed on param is the old one
        /// </summary> 
        public List<Item> GetNewItems(Feed feed)
        {
            var items = new List<Item>();
            for(int i = 0; i < GetNumberOfNewIndexes(feed); i++)
            {
                items.Add(Items[i]);
            }
            return items;
        }

        /// <summary>
        ///     Retrieves a the number of new indexes, based on the given feed
        /// </summary> 
        public int GetNumberOfNewIndexes(Feed feed)
        {
            if (Compare(feed))
            {
                return 0;
            }
            for (int i = 0; i < Items.Count; i++)
            {
                if (feed.Items[0].Title == Items[i].Title)
                {
                    return i + 1;
                }
            }
            return Items.Count;
        }

        /// <summary>
        ///     Compares with another feed
        ///     <para>Return true if they are equal. False if they are not</para>
        /// </summary> 
        private bool Compare(Feed feed)
        {
            if(Items.Count != feed.Items.Count)
            {
                return false;
            }

            for(int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Title != feed.Items[i].Title)
                {
                    return false;
                }
            }
            return true;

        }

    }
}
