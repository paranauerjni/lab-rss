﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LABProjetoRSS
{
    /// <summary>
    ///     Holds the standards for a Notification
    ///     <para>Can be more implemented on future</para>
    /// </summary> 
    public class Notification
    {
        public Item Item { get; set; }
        public string Key { get; set; }
        public bool Visualized { get; set; }

        public Notification()
        {

        }

        public Notification(Item item, string key)
        {
            Item = item;
            Key = key;
        }

    }
}
