﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LABProjetoRSS
{
    public class RSSHandler : XmlHandler
    {
        /// <summary>
        ///     Retrieve the RSS channel node
        /// </summary> 
        protected XmlNode GetChannelNode()
        {
            XmlNode rssNode = doc.DocumentElement;
            return rssNode.FirstChild;
        }
    }
}
