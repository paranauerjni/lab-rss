﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace LABProjetoRSS
{
    /// <summary>
    /// Lógica interna para Browser.xaml
    /// </summary>
    public partial class Browser : Window
    {

        public string Url { get { return Url; } set { var uri = new Uri(value); wb_data.Source = uri; } }

        public Browser()
        {
            InitializeComponent();
            HideScriptErrors(wb_data, true);
        }

        // Código retirado da net
        public void HideScriptErrors(WebBrowser wb, bool hide)
        {
            var fiComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            var objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null)
            {
                wb.Loaded += (o, s) => HideScriptErrors(wb, hide); //In case we are to early
                return;
            }
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { hide });
        }

    }
}
